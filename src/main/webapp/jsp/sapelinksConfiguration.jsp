<%@ page import="net.sapelinks.portlet.SapeLinksPortlet" %>
<%@ include file="./init.jsp" %>
<%
  PortletPreferences preferences = renderRequest.getPreferences();
  String portletResource = ParamUtil.getString(request, "portletResource");
  String sapeHost = StringPool.BLANK;
  String sapeUser = StringPool.BLANK;
  boolean isDebug = true;
  if (Validator.isNotNull(portletResource)) {
    preferences = PortletPreferencesFactoryUtil.getPortletSetup(request, portletResource);
    sapeHost = preferences.getValue(SapeLinksPortlet.SAPE_HOST, "");
    sapeUser = preferences.getValue(SapeLinksPortlet.SAPE_USER, "");
    isDebug = Boolean.parseBoolean(preferences.getValue(SapeLinksPortlet.IS_DEBUG, "true"));
  }
%>

<liferay-ui:success key="saved_successfully" message="sapelinks-saved-successfully"/>
<liferay-ui:error key="save_error" message="sapelinks-save-error"/>

<form action="<liferay-portlet:actionURL portletConfiguration="true" />" method="post" name="<portlet:namespace/>fm">

  <aui:input type="text" name="<%= SapeLinksPortlet.SAPE_HOST %>" value="<%= sapeHost %>" />
  <aui:input type="text" name="<%= SapeLinksPortlet.SAPE_USER %>" value="<%= sapeUser %>" />
  <aui:input type="checkbox" name="<%= SapeLinksPortlet.IS_DEBUG %>" value="<%= isDebug %>" />

  <input name="<portlet:namespace /><%=Constants.CMD%>" type="hidden" value="<%=Constants.UPDATE%>" />

  <aui:button type="submit" value="Save"/>
</form>