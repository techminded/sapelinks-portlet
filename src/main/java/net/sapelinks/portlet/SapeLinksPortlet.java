package net.sapelinks.portlet;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletPreferencesFactoryUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

import net.sapelinks.javasape.Sape;
import net.sapelinks.javasape.SapePageLinks;

import javax.portlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

public class SapeLinksPortlet extends MVCPortlet {

    private static final Log _log = LogFactoryUtil.getLog(SapeLinksPortlet.class);

    public static final String SAPE_USER = "sapeUser";
    public static final String SAPE_HOST = "sapeHost";
    public static final String IS_DEBUG = "isDebug";
    public static final String SAPE_LINKS_RENDER = "sapeLinksRender";

    @Override
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
            throws PortletException, IOException {

        HttpServletRequest request = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));

        ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

        String sapeUser =  StringPool.BLANK;
        String sapeHost =  StringPool.BLANK;
        boolean isDebug = true;
        try {
            PortletPreferences prefs = renderRequest.getPreferences();
            sapeUser = prefs.getValue(SAPE_USER, "");
            sapeHost = prefs.getValue(SAPE_HOST, "");
            isDebug = Boolean.parseBoolean(prefs.getValue(IS_DEBUG, "true"));
        } catch (Exception e) {
            _log.error("Error getting portlet settings", e);
        }

        Sape sape = new Sape(sapeUser, sapeHost, 0, 0);
        sape.debug = isDebug;

        SapePageLinks sapePageLinks = sape.getPageLinks(request.getRequestURI(), request.getCookies());

        renderResponse.setContentType(renderRequest.getResponseContentType());

        PrintWriter writer = renderResponse.getWriter();

        writer.write(sapePageLinks.render());
    }


    /**
     * @param request
     * @param response
     */
    private void redirect(ActionRequest request, ActionResponse response) {
        String redirectUrl = ParamUtil.getString(request, "redirect");
        if (redirectUrl != null && redirectUrl.trim().length() > 0) {
            try {
                response.sendRedirect(redirectUrl);
            } catch (IOException e) {
                _log.error("Failed to redirect to " + redirectUrl, e);
            }
        }
    }
}
