package net.sapelinks.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portlet.PortletPreferencesFactoryUtil;

public class SapeLinksPortletConfigurationAction extends DefaultConfigurationAction {

    @Override
    public void processAction(PortletConfig portletConfig,
                              ActionRequest actionRequest, ActionResponse actionResponse)
            throws Exception {
        String portletResource = ParamUtil.getString(actionRequest, "portletResource");
        PortletPreferences prefs = PortletPreferencesFactoryUtil.getPortletSetup(actionRequest, portletResource);

        String sapeHost = ParamUtil.getString(actionRequest, SapeLinksPortlet.SAPE_HOST, StringPool.BLANK);
        if (!sapeHost.equals(StringPool.BLANK)){
            prefs.setValue(SapeLinksPortlet.SAPE_HOST, sapeHost);
        }
        String sapeUser = ParamUtil.getString(actionRequest, SapeLinksPortlet.SAPE_USER, StringPool.BLANK);
        if (!sapeUser.equals(StringPool.BLANK)){
            prefs.setValue(SapeLinksPortlet.SAPE_USER, sapeUser);
        }
        boolean isDebug = Boolean.parseBoolean(ParamUtil.getString(actionRequest, SapeLinksPortlet.IS_DEBUG, "true"));

        prefs.setValue(SapeLinksPortlet.IS_DEBUG, String.valueOf(isDebug));

        prefs.store();
        SessionMessages.add(actionRequest, "saved_successfully");
    }

    @Override
    public String render(PortletConfig portletConfig,
                         RenderRequest renderRequest, RenderResponse renderResponse)
            throws Exception {
        return "/jsp/sapelinksConfiguration.jsp";
    }


}